using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float _speed;

    public GameObject SmallFirePrefab;

    private GameObject canoPrimer;
    private GameObject canoSegon;
    private Vector3 firstT;
    // Start is called before the first frame update
    void Start()
    {
        _speed = Time.deltaTime / 2;
        canoPrimer = GameObject.Find("2DFighterCanon");
        canoSegon= GameObject.Find("2DFighterCanon2");
        firstT = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
      
            MovementPlayer();

        if (Input.GetKeyDown("space"))
        {
            Disparar();
        }
        
    }

    void MovementPlayer()
    {
        transform.position = new Vector3(_speed * Input.GetAxis("Horizontal")+ transform.position.x, _speed * Input.GetAxis("Vertical") + transform.position.y, transform.position.z);

    }
    void Disparar()
    {
        Vector3 newPos = canoPrimer.transform.position;
        Vector3 newPos2 = canoSegon.transform.position;
        Instantiate(SmallFirePrefab, newPos, Quaternion.identity);
        Instantiate(SmallFirePrefab, newPos2, Quaternion.identity);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "FuegoteEnemigo")
        {
            Debug.Log("me choqu�!");

            // collision.gameObject.GetComponent<PlayerData>().SetFuegote();

            Destroy(gameObject);
            //Instantiate(gameObject, firstT, transform.rotation);

            

            // _animator.GetCurrentAnimatorStateInfo(0).normalizedTime
            // Destroy(this.gameObject); //Destuir en memoria

        }

  
    }

}
