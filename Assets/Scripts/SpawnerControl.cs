using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerControl : MonoBehaviour
{
    public float _minRandom, maxRandom;
    public GameObject AlienSpeed;
    public GameObject AlienSpeedSlow;

    float tiempo = 0;
    float intervalo = 0;
    float framesTotal = 3;

    // Start is called before the first frame update
    void Start()
    {
        _minRandom = -12f;
        maxRandom = 13f;
        
    }

    // Update is called once per frame
    void Update()
    {


        tiempo += Time.deltaTime;

        if (tiempo >= 1 / framesTotal)
        {
            intervalo++;
            tiempo = 0;

            if (intervalo == 3)
            {
               
                
               
                Vector3 newPos = new Vector3(Random.Range(_minRandom, maxRandom), 9, 0);
                Instantiate(AlienSpeed, newPos, Quaternion.identity);
                Vector3 newPos2 = new Vector3(Random.Range(_minRandom, maxRandom), 9, 0);
                Instantiate(AlienSpeedSlow, newPos2, Quaternion.identity);
                intervalo = 0;
            }
        }


    }
}
